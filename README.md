# Docs x résidences : gabarits // templates

--- ENGLISH FOLLOWS ---

## Pourquoi des gabarits ?

Pour servir de support à la documentation des résidences.

Pour structurer la rédaction.

Pour mieux comprendre les questions que l'on se pose et auxquelles on tente de répondre avec la documentation.

## Liste des gabarits

#### Essentiels

- index.md

Une fiche-contact qui sert aussi de table des matières au paquet de documentation.

- artsmatters.md

Documentation artistique du projet, plutôt générale.

- journal.md et journal-exemple.md

Gabarit pour un journal de bord, avec un exemple fictif complété.


#### Optionnels

- blog.md

Pour rédiger un billet de blogue.

- codesnippets.md

Prendre des notes pour contextualiser les extraits de code.

- science.md

Pour le contenu à saveur scientifique - à voir avec votre contact technique au Metalab.

- techreport.md

Rapport d'utilisation d'une technique spécifique.

- techtutorial.md (à venir)

Partager ses connaissances dans un tutoriel.

- zine.md (à venir)

Partager son expérience en tant qu'artiste en résidence dans un format papier.

--------------------------------------------------------------------------------

## Why use templates?

As a support for documenting residencies.

To help structure the writing process.

To better understand the questions we do have and to which we try to answer by documenting.

## Templates list

#### Essentials

- index.md

A file with contact information that doubles as a table of contents for the documentation package.

- artsmatters.md

Artistic documentation of the project, from a general point of view.

- journal.md and journal-exemple.md

Templates for a log journal, with a complete, fictive example.

#### Optional

- blog.md

To write a blog post.

- codesnippets.md

Taking some notes to help put in context the code snippets.

- science.md

Some science content - ask your technical contact at the Metalab.

- techreport.md (soon)

A report about using a specific technical component.

- zine.md (soon)

Share your experience as an artist in residence on paper.

