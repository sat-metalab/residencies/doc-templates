# Log journal - guide

GOAL: Keep track of technical and artistic experiments, in a semi-structured way that aims to be exhaustive.

Filled every day or every week.

## Before

The day starts - we take some notes on what we want to do, our plan on how to achieve it, and what are wished for results.

We can come back to this section through the day to help us refocus on what we want to do.

In this section, write down:

* a general goal, and maybe 2-3 daily sub-goals related to the general one: I want to do...
* hypothesis linked to the work to be done: I think that...
* steps that you will take for every goal or sub-goal: I will try...
        
## During

The goal of planning is the process, not the plan - so we take notes while we execute the plan in order to improve it - or to change it.

As the day continues, or at specific time, we take some time to note down what was achieved, which obstacles we faced... and which solutions we found.

In this section, write down:

* the step that was addressed, or the hypothesis that was explored
* what was planned and what we did about it
* what we learned from the things we did
* planning for the next step

## After

After a good work session, it's time to do some thinking.

We revisit the goals, and we think about what went well or what could be tried again tomorrow, or what needs to be left behind.

In this section, write down:

* how far or close are we from the goal
* which hypothesis were accurate, what we learned
* list of steps we took from those who were planned
* some notes for tomorrow

