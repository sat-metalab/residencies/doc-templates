# Arts matters - guide

## Information générale

Nom, prénom
Titre du projet
Lieu
Médiums
Outils

## Description artistique du projet


## Description technique du projet


## Démarche artistique


## Principaux défis techniques ou artistiques en lien avec la résidence du Metalab


## Biographie de l'artiste ou du collectif


## Liens pertinents


