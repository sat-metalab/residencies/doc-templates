# Journal de bord - guide

OBJECTIF: garder une trace des expérimentations techniques ou artistiques, de façon semi-structurée mais qui se veut exhaustive.

À remplir de façon quotidienne ou hebdomadaire.

## Avant

La journée commence - on prend des notes sur ce que l'on souhaite accomplir, comment on planifie s'y prendre, et quels sont les résultats escomptés.

On peut revenir sur cette section tout au long de la journée pour se recentrer sur ce que l'on souhaite accomplir.

À inscrire:

* un objectif général, et possiblement 2-3 sous-objectifs par jour pour accomplir la tâche principale: je souhaite accomplir...
* les hypothèses en lien avec le travail à accomplir - je pense que...
* les étapes qui seront entreprises pour chaque objectif ou sous-objectif - je tenterai de faire...
        
## Pendant

L'important dans la planification, c'est le processus, pas le plan - donc on prend des notes pendant l'exécution du plan pour l'améliorer ou justifier de changer le plan.

Tout au long de la journée ou à des moments particuliers, on prend une pause pour noter les avancées, les obstacles... et les solutions trouvées.

À inscrire:

* l'étape concernée, ou encore, l'hypothèse explorée
* ce que l'on a fait en lien avec le plan
* ce que l'on a appris des actions entreprises
* planification pour la prochaine action

## Après

Après une bonne séance de travail, c'est le moment de faire le bilan.

On revisite les objectifs, et on voit ce qui a bien fonctionné et ce qui est à retenter le lendemain, ou à abandonner.

À inscrire:

* niveau d'atteinte des objectifs
* état de connaissance des hypothèses: qu'est-ce qui a tenu la route, ce qu'on a appris
* liste des étapes entreprises parmi celles planifiées
* brève liste de notes pour le lendemain


