# Arts matters - guide

## General information

Name, first name
Project's title
Location
Mediums
Tools

## Artistic description of the project


## Technical description of the project


## Artistic process or statement


## Main technical or artistic challenges related to the Metalab residence


## Biography of the artist or of the collective


## Relevant links


