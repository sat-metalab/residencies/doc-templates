# Code snippets - guide

## Introduction

Overview of the context, in natural language, of the code we wrote.

Why we wrote this code, what does one use it for.

## Requirements

Technical requirements for being able to use the code.

## Code

Please add comments explaining the what and the why of the code, if the code is more than a few lines.

## How to use this code

In which context the code can be made useful.

## Conclusion

If you want to know more, references.

## Licence
