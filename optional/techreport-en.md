# Technical report - guide

A technical use case report.

## Goal achieved with or by the technique

Why is this technique or technology interesting, what we wish to accomplish with it.

## Describe the technique you wish to test

The context surrounding its usage, why we want to test it...

## Plan the test

Questions we wish to answer related to the technique with the test we are about to do.

Testing steps: what we will do and how we will do it.

## Test notes

Any note related to the test undertaken, written down while running the test.

## Results: an analysis

Challenges you faced, how we did or did not achieve the goal we set for ourselves.

## Next steps

Other possible tests, or any other step related to the current goal.


