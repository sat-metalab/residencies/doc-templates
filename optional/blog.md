# Billet de blog - guide

## Introduction

Qui, quand, quoi, comment, pourquoi - pas nécessairement dans cet ordre.

Une photo ou une vidéo, un extrait sonore.

## Un aspect du projet à explorer

Ce peut être une composante technique ou une question artistique.

## Comment on planifie l'explorer

Les hypothèses émises avant l'exploration et les étapes planifiées pour arriver à une réponse.

## Déroulement

Comment on a implémenter chaque étape, les obstacles rencontrés, comment on les a surmontés.

## Dénouement

Le résultat final, retour sur les hypothèses de départ.


## Conclusion

Prochaines étapes, où on peut voir le résultat, etc.

## Contact

Liens ou courriel pour en savoir plus
