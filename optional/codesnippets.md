# Code snippets - guide

## Introduction

Contexte haut-niveau, en langage humain, du code que l'on écrit.

Pourquoi on a écrit ce code, à quoi il sert.

## Dépendances

Dépendances informatiques pour pouvoir utiliser le code.

## Code

Ne pas oublier d'ajouter des commentaires expliquant le comment et le pourquoi si le code fait plus qu'une dizaine de lignes.

## Comment utiliser ce code

Dans quel contexte ce code peut être utile.

## Conclusion

Pour en savoir plus, références

## Licence
