# Rapport technique - guide

Rapport d'utilisation d'une technique.

## Objectif de la technique

Pourquoi on s'intéresse à cette technique, ce qu'on souhaite accomplir avec celle-ci.

## Description de la technique que l'on souhaite tester

Contexte d'utilisation, pourquoi on la teste...

## Planification du test

Les questions auxquelles on souhaite répondre en rapport à cette technique par le test que l'on s'apprête à effectuer.

Étapes du test: ce qu'on va faire et comment on va le faire.

## Notes de test

Toute note pertinente en lien avec le test effectué, prises au cours de la réalisation.

## Analyse des résultats

Obstacles rencontrés, retour à l'objectif: a-t-on réussi ce qu'on voulait accomplir ou non ?

## Prochaines étapes

Autres tests possibles, ou tout autre étape en lien avec le présent objectif.



