# Blog post - guide

## Introduction

Whom, when, what, how, why - doesn't have to be in this order.

A picture or a video, a sound sample.

## One thing about the project we want to explore

It might be a technical component, or something to ponder artistically.

## How are we planning to do the exploration ?

The hypothesis we had before doing the exploration, and the steps we planned to obtain an answer.

## Doing the exploration

How each step was implemented, challenges that we faced, how we manage them.

## Exploration: results

Which knowledge was created by the exploration. Reflecting on the hypothesis we started with.

## Conclusion

Next steps, where can we see the results, etc.

## Contact

Links and/or email to know more about this.
