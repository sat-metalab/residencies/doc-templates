# Log journal - guide

GOAL: keep some notes about technical and artistic experimentations, in a semi-structured way who aims to be exhaustive.

To be filled up weekly or daily.

## Before

Main goal: to understand how to use the OSC protocol in my project - features and limits.

### Secondary goals

* clarify my understanding of the OSC protocol in general
* find a concrete use case example of the OSC protocol
* reproduce this example as a way to make sure that my understanding of the OSC protocol is accurate.

### Hypothesis

* the OSC protocol allows the exchange of data types that are more general than MIDI
* I will be able to find a good example of using the OSC protocol

### Steps

* find documentation about the OSC protocol in general
* read it and write down an abstract of it in my own words
* find on Gitlab, Github or on the Internet in general, some use cases of OSC in contexts similar to mine
* read the use case, write down the questions I have so far about it
* reproduce the example in an application or a programming language that I know well
* revisit the general documentation on the OSC protocol to help clarify my understanding and make links between the example and the theory
        
## During

Step: find documentation on the OSC protocol in general

What I've done so far: look on the internet for technical specifications of the protocol

I've learned:

* the protocol is complex!
* I think I understand what type of message is sent/received: here is an example
* I am not sure I understand how to encode a message

Next action item: read on encoding for OSC messages, and think about how it applies to the type of messages that I wish to send.

## After

General goal: close to completion, I now have a better understanding of the limitations of this protocol, but the features are still a bit blurry.

Hypothesis:

* It's true that the OSC protocol allows for the exchange of types of data that are more general than the MIDI protocol...
* ... but it was complex to find an example that tells everything I need to know about this

Steps undertaken:

* I found documentation
* I wrote an abstract (here included)
* I found an example of a use case on Gitlab
* I've read it and took some notes
* that's all for today!

Steps for tomorrow:

* to understand completely the chosen use case
* to adapt it to the needs of my project

Notes for tomorrow:

* ask a Metalab team member if they can coach me on how to adapt the protocol to my needs
* check the type of OSC message sent/received by the Metalab tool that I use
