# Journal de bord - guide

OBJECTIF: garder une trace des expérimentations techniques ou artistiques, de façon semi-structurée mais qui se veut exhaustive.

À remplir de façon quotidienne ou hebdomadaire.

## Avant

Objectif général: comprendre comment utiliser le protocole OSC dans le cadre de mon projet: possibilités et limitations.

### Sous-objectifs

* améliorer ma compréhension du protocole OSC en général
* trouver un exemple concret d'utilisation du protocole OSC
* reproduire cet exemple afin de confirmer ma compréhension du protocole OSC

### Hypothèses

* le protocole OSC permet d'échanger des types de données plus générales que le protocole MIDI
* je pourrai trouver un bon exemple d'utilisation du protocole OSC

### Étapes

* trouver de la documentation sur le protocole OSC en général
* la lire et en faire un résumé dans mes mots
* rechercher sur Gitlab ou sur Github, ou sur Internet en général, des exemples d'utilisation d'OSC dans des contextes qui sont similaires au mien
* lire l'exemple d'utilisation, prendre mes questions en note
* le reproduire dans une application ou un langage de programmation que je connais
* relire la documentation générale sur le protocole OSC pour parfaire ma compréhension et faire des liens entre l'exemple et la théorie
        
## Pendant

Étape: trouver de la documentation sur le protocole OSC en général

Ce que j'ai fait: recherches sur internet des spécifications techniques du protocole

J'ai appris:

* le protocole est complexe !
* je pense comprendre le type de message qu'il reçoit/envoit: voici un exemple
* je ne suis pas certaine de comprendre comment encoder un message

Prochaine action: lire sur l'encodage de messages OSC, et voir comment ça s'applique au type de messages que je souhaite envoyer.

## Après

Objectif général: relativement atteint, j'ai une meilleure compréhension des limites du protocole OSC mais les possibilités sont encore floues.

Hypothèses:

* il est vrai que le protocole OSC permet d'échanger des données de type plus général que le protocole MIDI...
* ... mais c'était complexe de trouver un exemple pour illustrer ce propos

Étapes entreprises:

* j'ai trouvé de la documentation
* je l'ai résumée (inclure le résumé)
* j'ai trouvé un exemple  d'utilisation sur Gitlab
* je l'ai lu et annoté
* c'est tout pour aujourd'hui

Étapes pour demain:

* comprendre et maîtriser l'exemple d'utilisation
* l'adapter à mes besoins pour mon projet.

Notes pour demain:

* demander à un membre du Metalab de m'accompagner dans l'adaptation du protocole à mes besoins
* vérifier le type de messages OSC utilisés par l'outil du Metalab dont j'ai besoin
